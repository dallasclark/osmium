export const capabilities = {
  browserName: "CHROME"
};

export const defaultTimeout: number = 30 * 1000;

export const resultsDirectory: string = "results";
