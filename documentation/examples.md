# Osmium

## Examples

Getting started with Osmium is easy, simply follow the examples below to learn how to setup your features.

1. [Google and Bing Search - Separate Windows](../features/google-and-bing-search-separate-windows.feature)
1. [Google and Bing Search - Same Window](../features/google-and-bing-search-same-window.feature)
1. [Alias References](../features/alias-references.feature)
1. [Multiple Test Scenarios using Example Data](../features/example-data.feature)
1. [Screenshots](../features/screenshot.feature)
1. [Failing Scenario Example](../features/failing-feature.feature)
1. [Tags](../features/tags.feature)
