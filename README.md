# Osmium

Osmium (_oz-mee-uh m_) is a testing framework that makes creating Automated Tests suck less.

The framework promotes the use of general Gherkin/Cucumber references that are agnostic to any product or service. Allowing all users (technical or non-technical) to start testing with very little effort.

## Big Thanks

Cross-browser Testing Platform and Open Source ❤️ provided by [Sauce Labs](https://saucelabs.com).

## Installation

### Pre-Requisites

1. Ensure you have Node v8+

   `node -v`

### Instructions

1. Clone the project to your computer

1. Change directory to the project's folder

   `cd osmium`

1. Install dependancies

   `npm install`

## Setup

TODO

## Usage

### E2E Automated Testing

Execute the E2E (End to End) Automated Tests

`npm test`
