const cowsay = require("cowsay");
import { writeFile } from "fs";

import { currentTestDirectory } from "../";

interface IScreenshotParameters {
  callback?: Function;
  driver: any;
}

/**
 * @description
 * Takes a screenshot using the driver provided
 *
 * @param driver
 * @param {function} callback The function to callback when the screenshot is complete
 *
 * @returns {Object}
 */
const takeScreenshot = async (parameters: IScreenshotParameters) => {
  const filename = `${currentTestDirectory}/screenshot-${Date.now()}.png`;
  const screenshot = await parameters.driver.takeScreenshot();

  await writeFile(filename, screenshot, "base64", (error: any) => {
    if (error) {
      console.error(
        cowsay.say({
          text: "Screenshot ERROR"
        })
      );
      console.error(error);
    }
  });

  return {
    filename
  };
};

export { takeScreenshot };
