const cowsay = require("cowsay");
import { After, Status } from "cucumber";

import { takeScreenshot } from "./screenshots";
import { driver, driverQuit } from "../step-definitions/browser";

After(async (scenario: any) => {
  if (scenario.result.status === Status.FAILED) {
    const screenshot = await takeScreenshot({
      driver
    });

    console.error(
      cowsay.say({
        text: "Scenario FAILED"
      })
    );
    console.error({
      name: scenario.pickle.name,
      duration: scenario.result.duration,
      exception: scenario.result.exception,
      screenshot: {
        filename: screenshot.filename
      },
      sourceLocation: {
        uri: scenario.sourceLocation.uri,
        line: scenario.sourceLocation.line
      }
    });

    return driverQuit();
  }
});
