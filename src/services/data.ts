import { Before } from "cucumber";

const assert = require("assert");

/**
 * @description
 * The key used for storing alias data
 */
const ALIAS_KEY = "alias";

interface IAliasDataTableItem {
  [id: string]: string;
}

interface IData {
  [ALIAS_KEY]: IAliasDataTableItem;
}

interface IAliasRawDataTableItem {
  [id: number]: string;
  length: number;
}

interface IAliasRawDataTableItems extends Array<IAliasRawDataTableItem> {}

/**
 * @description
 * Data object to store all attributes used throughout tests
 */
const data: IData = {
  [ALIAS_KEY]: Object()
};

Before(() => {
  aliasBulkDelete();
});

/**
 * @description
 * Creates an alias data object at the given key
 *
 * @param {string} key The key to store the value at
 * @param {string} value The value of the alias
 */
const aliasCreate = (key: string, value: string) => {
  assert(key.length > 0, "The key should not be blank");
  assert(
    value.length > 0,
    "The value of the alias element should not be blank"
  );
  assert(
    typeof data[ALIAS_KEY][key] === "undefined",
    "The alias already exists"
  );

  data[ALIAS_KEY][key] = value;
};

/**
 * @description
 * Reads the alias from teh alias data object
 *
 * @param {string} key The alias to retrieve
 */
const aliasRead = (key: string) => {
  assert(
    typeof data[ALIAS_KEY][key] !== "undefined",
    "The alias does not exist"
  );

  return data[ALIAS_KEY][key];
};

/**
 * @description
 * Updates an alias data object at the given key
 *
 * @param {string} key The key to store the value at
 * @param {string} value The value of the alias
 */
const aliasUpdate = (key: string, value: string) => {
  assert(
    typeof data[ALIAS_KEY][key] !== "undefined",
    "The alias does not exist"
  );

  data[ALIAS_KEY][key] = value;
};

/**
 * @description
 * Deletes the alias data object at the given key
 *
 * @param {string} key The key to delete
 */
const aliasDelete = (key: string) => {
  assert(
    typeof data[ALIAS_KEY][key] !== "undefined",
    "The alias does not exist"
  );

  data[ALIAS_KEY] = {};
};

/**
 * @description
 * Creates multiple alias data objects at the given keys
 *
 * @param {IAliasRawDataTableItems} aliasDataTable
 */
const aliasBulkCreate = (aliasDataTable: IAliasRawDataTableItems) => {
  aliasDataTable.forEach((alias: IAliasRawDataTableItem) => {
    // The length of aliases should be 2 to form the key:value pair
    assert(
      alias.length === 2,
      "The data provided for an alias should only contain 2 items"
    );

    aliasCreate(alias[0], alias[1]);
  });
};

/**
 * @description
 * Updates multiple alias data objects at the given keys
 *
 * @param {IAliasRawDataTableItems} aliasDataTable
 */
const aliasBulkUpdate = (aliasDataTable: IAliasRawDataTableItems) => {
  aliasDataTable.forEach((alias: IAliasRawDataTableItem) => {
    // The length of aliases should be 2 to form the key:value pair
    assert(
      alias.length === 2,
      "The data provided for an alias should only contain 2 items"
    );

    aliasUpdate(alias[0], alias[1]);
  });
};

/**
 * @description
 * Deletes multiple data objects by the given alias keys.
 *
 * If no alias keys exist, all aliases are deleted.
 *
 * @param {string[]} keys The keys to delete (optional)
 */
const aliasBulkDelete = (keys?: string[]) => {
  if (!keys || !keys.length) {
    data[ALIAS_KEY] = {};
  } else {
    keys.forEach(key => {
      aliasDelete(key);
    });
  }
};

export {
  aliasCreate,
  aliasRead,
  aliasUpdate,
  aliasDelete,
  aliasBulkCreate,
  aliasBulkUpdate,
  aliasBulkDelete
};
