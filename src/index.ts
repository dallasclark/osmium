import {
  BeforeAll,
  setDefaultTimeout as cucumberSetDefaultTimeout
} from "cucumber";
import { existsSync, mkdirSync } from "fs";

import { defaultTimeout, resultsDirectory } from "../config";

const currentTestDirectory: string = `${resultsDirectory}/${Date.now()}`;

cucumberSetDefaultTimeout(defaultTimeout);

// Create results directory if it doesn't exist
if (!existsSync(resultsDirectory)) {
  mkdirSync(resultsDirectory);
}

BeforeAll(() => {
  // Create current test directory
  mkdirSync(currentTestDirectory);
});

export { currentTestDirectory };
