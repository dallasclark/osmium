import { defineParameterType } from "cucumber";

export enum ISelectorType {
  className = "className",
  css = "css",
  id = "id",
  linkText = "linkText",
  name = "name",
  partialLinkText = "partialLinkText",
  tagName = "tagName",
  xpath = "xpath"
}

defineParameterType({
  name: "selectorType",
  regexp: /className|css|id|linkText|name|partialLinkText|tagName|xpath/,
  transformer: selectorType => selectorType
});
