import { defineParameterType } from "cucumber";

defineParameterType({
  name: "alias",
  regexp: /(alias )?/,
  transformer: alias => alias
});
