import { Then } from "cucumber";
import { By, until } from "selenium-webdriver";

import { driver } from "../browser";
import { aliasRead as dataAliasRead } from "../../services/data";

/**
 * @description
 * Resolves if an item on the page contains text
 *
 * @param {string} alias
 * @param {string} text
 */
Then(
  "I wait for {alias}{string} to be displayed",
  async (alias: string, text: string) => {
    if (typeof alias !== "undefined") {
      text = dataAliasRead(text);
    }

    text = `//*[contains(text(), '${text}')]`;
    return await driver.wait(until.elementsLocated(By.xpath(text)));
  }
);
