import { Then } from "cucumber";

import { driver } from "../browser";
import { takeScreenshot } from "../../services/screenshots";

Then(`I take a screenshot`, () => {
  return takeScreenshot({
    driver
  });
});
