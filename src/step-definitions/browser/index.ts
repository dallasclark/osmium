import { Given, Then } from "cucumber";
import { Builder, Capabilities } from "selenium-webdriver";

// TODO Setup proper explicit typing on driver
export let driver: any;

/**
 * @description
 * Launches a new browser window
 */
Given("I launch a new window", async () => {
  // TODO: Launch browser based on configuration
  // @link https://trello.com/c/WekO383N/1-trigger-different-browser-based

  // TODO: Check if the browser already exists
  return await driverBuild();
});

/**
 * @description
 * Navigates to the URL provided
 */
Given("I navigate to the {string} Website", async websiteUrl => {
  return await driver.get(websiteUrl);
});

/**
 * @description
 * Closes the current window
 */
Then("I close the current window", async () => {
  return await driverQuit();
});

/**
 * @description
 * Behaviours to perform when building the driver
 */
const driverBuild = async () => {
  return (driver = await new Builder()
    .forBrowser("chrome")
    .withCapabilities(Capabilities.chrome())
    .build());
};

/**
 * @description
 * Behaviours to perform when quiting the driver
 */
const driverQuit = async () => {
  return await driver.quit();
};

export { driverBuild, driverQuit };
