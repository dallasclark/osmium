import { When } from "cucumber";
import { By, Key } from "selenium-webdriver";

import { driver } from "../browser";
import { ARTICLE } from "../../constants/cucumber-syntax";
import { ISelectorType } from "../../parameter-types/selector-types";
import { aliasRead as dataAliasRead } from "../../services/data";

/**
 * @description
 * Clears an element's content
 *
 * @example
 * When I clear the element with the name of "q"
 *
 * @param {string} selectorType The type of element to find (@link ../../parameter-types/selector-types.ts)
 * @param {string} alias Whether the selector value is an alias
 * @param {string} selectorValue The value used to find the element
 */
When(
  `I clear ${ARTICLE} element with ${ARTICLE} {selectorType} of {alias}{string}`,
  async (selectorType: ISelectorType, alias: string, selectorValue: string) => {
    if (typeof alias !== "undefined") {
      selectorValue = dataAliasRead(selectorValue);
    }

    /**
     * @description
     * The selector found by type of element and unique identifier
     */
    const selector = By[selectorType](selectorValue);

    // Find the element and send all keys
    return await driver.findElement(selector).clear();
  }
);

/**
 * @description
 * Populate an element with content and send additional keys on complete.
 *
 * @example
 * When I populate the element with the "name" of "q" with "Automated Testing" and press "ENTER"
 *
 * @param {string} selectorType The type of element to find (@link ../../parameter-types/selector-types.ts)
 * @param {string} selectorValue The value used to find the element
 * @param {string} content The content to populate
 * @param {string} additionalKey Additional key to press after the content is inserted
 */
When(
  `I populate ${ARTICLE} element with ${ARTICLE} {selectorType} of {alias}{string} with {alias}{string} and press {string}`,
  async (
    selectorType: ISelectorType,
    selectorAlias: string,
    selectorValue: string,
    contentAlias: string,
    content: string,
    additionalKey: string
  ) => {
    if (typeof selectorAlias !== "undefined") {
      selectorValue = dataAliasRead(selectorValue);
    }

    if (typeof contentAlias !== "undefined") {
      content = dataAliasRead(content);
    }

    /**
     * @description
     * The selector found by type of element and unique identifier
     */
    const selector = By[selectorType](selectorValue);

    /**
     * @description
     * The keys to send to the selector
     */
    const keys = [content];

    /*
     * Checks if the Additional Key is the "ENTER" key
     * TODO: Check if the additional key is in the Key object (eg; Key.ENTER)
     */
    if (additionalKey === "ENTER") {
      keys.push(Key.ENTER);
    }

    // Find the element and send all keys
    return await driver.findElement(selector).sendKeys(...keys);
  }
);
