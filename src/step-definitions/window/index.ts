import { When } from "cucumber";

import { driver } from "../browser";

/**
 * @description
 * Moves to another frame
 *
 * @example
 * When I switch to frame "frameName"
 *
 * @param {string} frameName The name of the frame to switch to
 */
When(`I switch to frame {string}`, async (frameName: string) => {
  return await driver.switchTo().frame(frameName);
});

/**
 * @description
 * Moves to another window
 *
 * @example
 * When I switch to window "windowName"
 *
 * @param {string} windowName The name of the window to switch to
 */
When(`I switch to window {string}`, async (windowName: string) => {
  return await driver.switchTo().window(windowName);
});
