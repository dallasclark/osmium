import { Given } from "cucumber";

import { aliasBulkCreate as dataAliasBulkCreate } from "../../services/data";

Given("the alias mappings", dataTable => {
  dataAliasBulkCreate(dataTable.rawTable);
});
