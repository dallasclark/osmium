import { When } from "cucumber";
import { By } from "selenium-webdriver";

import { driver } from "../browser";
import { ARTICLE } from "../../constants/cucumber-syntax";
import { ISelectorType } from "../../parameter-types/selector-types";

// And I click the element with the name of "btnK"
When(
  `I click ${ARTICLE} element with the {selectorType} of {string}`,
  async (selectorType: ISelectorType, selectorValue: string) => {
    /**
     * @description
     * The selector found by type of element and unique identifier
     */
    const selector = By[selectorType](selectorValue);

    // Find the element and click it
    return await driver.findElement(selector).click();
  }
);
