Feature: Simple Web Browser Tests

  As an Internet user
  In order to learn more about a particular topic
  I want to be able to search and find details about topics

  # Take note, to use "Examples", "Scenario" must be labelled as "Scenario Outline"
  Scenario Outline: Launch and Search
    Given I launch a new window

    And I navigate to the "<search-engine>" Website
    When I populate the element with the name of "q" with "<search-query-01>" and press "ENTER"
    Then I wait for "<search-query-01>" to be displayed

    When I clear the element with the name of "q"
    And I populate the element with the name of "q" with "<search-query-02>" and press "ENTER"
    Then I wait for "<search-query-02>" to be displayed

    Then I close the current window

  Examples:
    | search-engine          | search-query-01   | search-query-02   |
    | https://www.google.com | automated testing | cucumber testing  |
    | https://www.bing.com   | cucumber testing  | automated testing |