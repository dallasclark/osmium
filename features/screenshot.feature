Feature: Simple Web Browser Tests

  As an Internet user
  In order to learn more about a particular topic
  I want to be able to search and find details about topics

  Scenario: Launch and Search on Google
    Given the alias mappings
      | SearchInputElement | q                  |
      | SearchInputValue1  | automated testing  |

    Given I launch a new window
    And I navigate to the "https://www.google.com" Website

    Then I take a screenshot

    When I populate the element with the name of alias "SearchInputElement" with alias "SearchInputValue1" and press "ENTER"
    Then I wait for alias "SearchInputValue1" to be displayed

    Then I take a screenshot

    Then I close the current window