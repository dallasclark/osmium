Feature: Simple Web Browser Tests

  As an Internet user
  In order to learn more about a particular topic
  I want to be able to search and find details about topics

  Scenario: Launch and Search on Google
    Given the alias mappings
      | SearchInputElement | q                  |
      | SearchInputValue1  | automated testing  |
      | SearchInputValue2  | cucumber testing   |

    Given I launch a new window
    And I navigate to the "https://www.google.com" Website
    When I populate the element with the name of alias "SearchInputElement" with alias "SearchInputValue1" and press "ENTER"
    Then I wait for alias "SearchInputValue1" to be displayed

    When I clear the element with the name of alias "SearchInputElement"
    And I populate the element with the name of alias "SearchInputElement" with alias "SearchInputValue2" and press "ENTER"
    Then I wait for alias "SearchInputValue2" to be displayed

    Then I close the current window