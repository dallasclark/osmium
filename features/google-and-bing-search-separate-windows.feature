Feature: Simple Web Browser Tests

  As an Internet user
  In order to learn more about a particular topic
  I want to be able to search and find details about topics

  Scenario: Launch and Search on Google
    Given I launch a new window
    And I navigate to the "https://www.google.com" Website
    When I populate the element with the name of "q" with "automated testing" and press "ENTER"
    Then I wait for "automated testing" to be displayed

    When I clear the element with the name of "q"
    And I populate the element with the name of "q" with "cucumber testing" and press "ENTER"
    Then I wait for "cucumber" to be displayed

    Then I close the current window

  Scenario: Launch and Search on Bing
    Given I launch a new window
    And I navigate to the "https://www.bing.com" Website
    When I populate the element with the name of "q" with "automated testing" and press "ENTER"
    Then I wait for "automated testing" to be displayed

    When I clear the element with the name of "q"
    And I populate the element with the name of "q" with "cucumber gherkin syntax" and press "ENTER"
    Then I wait for "gherkin" to be displayed

    Then I close the current window